import pytest
from tsp import Place
from tsp.algorithms import AntColony, GreedyTSP, Ant

def test_place(tsp_obj):
    colony = AntColony()
    place = Place(0,1,1)
    ant = Ant(colony, place)
    
    assert ant.current == place
